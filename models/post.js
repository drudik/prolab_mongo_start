const path = require('path');
const mongoose = require('mongoose');

const { Schema } = mongoose;
const generalSchema = new Schema ( { 
    title: {type: String, default: '', maxlength: 100, unique: true},
    body: {type: String, default: 'Lorem text...'},
    author: {
        type: String,
    name: {type: String, default: '', maxlength: 100}, 
    email: {type: String, default: '', maxlength: 100},
    },
    comments: {
            body: { type: String, maxlength: 255}, 
            date: {type: Date},
            email: { type: String, default: '', maxlength: 100}, 
        },
    date: { 
        type: Date, 
        default: Date.now 
    }
});

const modelname = path.basename(__filename, '.js');
const model = mongoose.model(modelname, generalSchema);
module.exports = model;