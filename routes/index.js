const express = require('express');
const multer = require('multer');
const upload = multer();

const modelSave = require('../controllers/modelSave'); //use function call modelSave(); 
const find = require('../controllers/find'); //use function call find();
const findUpdate = require('../controllers/findUpdate'); //use function call findUpdate();

const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {  
  res.render('index');
});
 
router.post('/save', upload.none() , async (req, res, next) => {
  const data = req.body
  // await modelSave( );
  res.json({data: data});
});

module.exports = router;
