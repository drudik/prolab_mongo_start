const PostModel = require('../models/post') 

const find = async ( data ) => {
    const model = PostModel.findOne({title: data});
    const doc = await model.exec();
    console.log('find result = ',doc.title);
};

module.exports = find;
